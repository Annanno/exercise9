package com.example.exercise9

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.exercise9.databinding.FragmentFirstBinding
import com.example.exercise9.databinding.FragmentRegisterFirstBinding

class FirstFragment : Fragment() {
    private lateinit var binding: FragmentFirstBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFirstBinding.inflate(inflater, container, false)
        setListeners()
        return binding.root
    }

    private fun setListeners() {
        binding.signUpBtn.setOnClickListener {
            findNavController().navigate(R.id.action_firstFragment_to_registerFirstFragment22)

        }
        binding.logInBtn.setOnClickListener {
            findNavController().navigate(R.id.action_firstFragment_to_signInFragment)
        }
    }

}